 package com.oasisfeng.island.controller

import android.Manifest.permission.REQUEST_INSTALL_PACKAGES
import android.app.admin.DevicePolicyManager
import android.content.*
import android.content.Intent.EXTRA_INSTALLER_PACKAGE_NAME
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.content.pm.ApplicationInfo
import android.content.pm.LauncherApps
import android.content.pm.PackageManager.*
import android.content.res.Configuration.UI_MODE_NIGHT_MASK
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.*
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES
import android.os.Build.VERSION_CODES.O
import android.os.Build.VERSION_CODES.P
import android.util.Log
import android.widget.Toast
import androidx.annotation.IntDef
import androidx.annotation.RequiresApi
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshotFlow
import androidx.core.content.getSystemService
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.oasisfeng.android.app.Activities
import com.oasisfeng.android.content.pm.LauncherAppsCompat
import com.oasisfeng.android.google.GooglePlayStore
import com.oasisfeng.android.ui.Dialogs
import com.oasisfeng.android.ui.WebContent
import com.oasisfeng.android.util.Apps
import com.oasisfeng.island.Config
import com.oasisfeng.island.IslandNameManager
import com.oasisfeng.island.analytics.Analytics
import com.oasisfeng.island.analytics.analytics
import com.oasisfeng.island.clone.AppClonesBottomSheet
import com.oasisfeng.island.controller.IslandAppControl.launchSystemAppSettings
import com.oasisfeng.island.controller.IslandAppControl.unfreezeInitiallyFrozenSystemApp
import com.oasisfeng.island.data.IslandAppInfo
import com.oasisfeng.island.data.IslandAppListProvider
import com.oasisfeng.island.data.helper.hidden
import com.oasisfeng.island.data.helper.installed
import com.oasisfeng.island.data.helper.isSystem
import com.oasisfeng.island.data.helper.suspended
import com.oasisfeng.island.engine.IslandManager
import com.oasisfeng.island.engine.common.WellKnownPackages
import com.oasisfeng.island.installer.InstallerExtras
import com.oasisfeng.island.mobile.R
import com.oasisfeng.island.model.interactive
import com.oasisfeng.island.shuttle.Shuttle
import com.oasisfeng.island.ui.ModelBottomSheetFragment
import com.oasisfeng.island.util.*
import com.oasisfeng.island.util.Users.Companion.isParentProfile
import com.oasisfeng.island.util.Users.Companion.toId
import eu.chainfire.libsuperuser.Shell
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.withContext
import rikka.shizuku.Shizuku
import rikka.shizuku.Shizuku.UserServiceArgs
import rikka.shizuku.Shizuku.removeRequestPermissionResultListener
import java.util.*
import java.util.concurrent.ExecutionException
import java.util.stream.Collectors
import kotlin.annotation.AnnotationRetention.SOURCE
import kotlin.annotation.AnnotationTarget.TYPE

 /**
 * Controller for complex procedures of Island.
 *
 * Refactored by Oasis on 2018-9-30.
 */
class IslandAppClones(val activity: FragmentActivity, val vm: AndroidViewModel, val app: IslandAppInfo) {

	fun request() {
		val names = IslandNameManager.getAllNames(context)
		check(names.isNotEmpty()) { "No Island" }
		val targets: MutableMap<UserHandle, String> = LinkedHashMap(names.size + 1)
		targets[Users.parentProfile] = context.getString(com.oasisfeng.island.shared.R.string.mainland_name)
		targets.putAll(names)

		val shouldShowBadge: Boolean = targets.size > 2
		val icons: Map<UserHandle, Drawable> = targets.entries.stream().collect(Collectors.toMap({ obj: Map.Entry<UserHandle, String> -> obj.key }) { e: Map.Entry<UserHandle, String> ->
			val user = e.key
			val res = if (user.isParentProfile()) R.drawable.ic_portrait_24dp else R.drawable.ic_island_black_24dp
			val drawable: Drawable = context.getDrawable(res)!!
			val dark = (context.resources.configuration.uiMode and UI_MODE_NIGHT_MASK) == UI_MODE_NIGHT_YES
			drawable.setTint(context.getColor(if (dark) android.R.color.white else android.R.color.black))		// TODO: Decouple
			if (shouldShowBadge) Users.getUserBadgedIcon(context, drawable, user) else drawable })

		// REQUEST_INSTALL_PACKAGES is disallowed on Google Play Store, thus removed in the Google Play packaging.
		val isShizukuAvailable = try { Shizuku.getVersion() >= 11 } catch (e: RuntimeException) { false }
		val isShizukuReady = isShizukuAvailable && Shizuku.checkSelfPermission() == PERMISSION_GRANTED
		val isPlayStoreAvailable = Apps.of(context).isAvailable(GooglePlayStore.PACKAGE_NAME)
		val isKnownInstalledByPlayStore = isKnownInstalledByPlayStore(context, pkg)
		val isPlayStoreReady = targets.size <= 2 && isPlayStoreAvailableInProfiles(targets.keys)

		val fragment = ModelBottomSheetFragment()
		val alp = IslandAppListProvider.getInstance(context)
		val dialog = AppClonesBottomSheet(targets, icons, { user -> alp.isInstalled(pkg, user) }) { target, mode ->
			// The normal follow-up procedure goes here
			makeAppAvailable(target, mode)
			fragment.dismiss() }

		fragment.show(activity) {
			val defaultMode = when {
				isShizukuReady -> MODE_SHIZUKU
				isPlayStoreReady && (isKnownInstalledByPlayStore || ! isInstallerUsable()) -> MODE_PLAY_STORE
				else -> null }
			val mode = mutableStateOf(defaultMode)
			dialog.compose(showShizuku = isShizukuAvailable, showPlayStore = isPlayStoreAvailable, mode)

			snapshotFlow { mode.value }.onEach {    // When the choice of mode chip is changed
				if (it != MODE_SHIZUKU || Shizuku.checkSelfPermission() == PERMISSION_GRANTED) return@onEach
				Shizuku.addRequestPermissionResultListener(object: Shizuku.OnRequestPermissionResultListener { override fun onRequestPermissionResult(requestCode: Int, grantResult: Int) {
					removeRequestPermissionResultListener(this)
					if (grantResult != PERMISSION_GRANTED) mode.value = MODE_INSTALLER }})	// Uncheck the chip if permission is denied
				Shizuku.requestPermission(1)	// Request permission when the chip is checked
			}.launchIn(vm.viewModelScope) }
	}

	/** Either by unfreezing initially frozen (system) app, enabling disabled system app, or clone user app. */
	private fun makeAppAvailable(profile: UserHandle, mode: Int) {
		val target = IslandAppListProvider.getInstance(context)[pkg, profile]
		if (target != null && target.isHiddenSysIslandAppTreatedAsDisabled) {   // Frozen system app shown as disabled, just unfreeze it.
			if (unfreezeInitiallyFrozenSystemApp(target) == true)
				Toast.makeText(activity, context.getString(R.string.toast_successfully_cloned, app.label), Toast.LENGTH_SHORT).show()
		} else if (target != null && target.isInstalled && !target.enabled) {  // Disabled app may be shown as "removed"
			launchSystemAppSettings(target)
			Toast.makeText(activity, R.string.toast_enable_disabled_system_app, Toast.LENGTH_SHORT).show()
		} else vm.interactive(context) {
			cloneApp(app, profile, mode)
		}
	}

	private suspend fun cloneApp(source: IslandAppInfo, target: UserHandle, mode: @AppCloneMode Int) {
		if (target.isParentProfile() && isInstallerUsable()) @Suppress("DEPRECATION") // Only works in parent profile due to a bug in AOSP.
			return activity.startActivityForResult(Intent(Intent.ACTION_INSTALL_PACKAGE, Uri.fromParts("package", pkg, null)), 1)   // startActivityForResult() is required on Android U+ due to a bug in AOSP.

		val context = source.context(); val pkg = source.packageName
		if (source.isSystem) {
			analytics().event("clone_sys").with(Analytics.Param.ITEM_ID, pkg).send()

			val enabled = Shuttle(context, to = target).invoke(with = pkg) { DevicePolicies(this).enableSystemApp(it) }

			if (enabled) Toast.makeText(context, context.getString(R.string.toast_successfully_cloned, source.label), Toast.LENGTH_SHORT).show()
			else Toast.makeText(context, context.getString(R.string.toast_cannot_clone, source.label), Toast.LENGTH_LONG).show()
			return
		}

		if (mode == MODE_SHIZUKU && Shizuku.checkSelfPermission() == PERMISSION_GRANTED) {
			val component = ComponentName(context, PrivilegedRemoteWorker::class.java)
			val args = UserServiceArgs(component).daemon(false).processNameSuffix(pkg)
			Shizuku.bindUserService(args, object: ServiceConnection {
				override fun onServiceConnected(name: ComponentName, service: IBinder) {
					val data = Parcel.obtain().apply { writeString(pkg); writeInt(target.toId()) }
					val reply = Parcel.obtain()
					try {
						service.transact(IBinder.FIRST_CALL_TRANSACTION, data, reply, 0)
						val result = reply.readInt()
						if (result == 1)
							Toast.makeText(context, context.getString(R.string.toast_successfully_cloned, source.label), Toast.LENGTH_SHORT).show()
						else Toast.makeText(context, context.getString(R.string.toast_cannot_clone, source.label), Toast.LENGTH_LONG).show() }
					finally {
						data.recycle(); reply.recycle()
						Handler(Looper.getMainLooper()).post { Shizuku.unbindUserService(args, this, false) }}
				}

				override fun onServiceDisconnected(name: ComponentName?) {}
			})
			return
		}

		if (SDK_INT >= O && cloneAppViaRoot(context, source, target)) return    // Prefer root routine to avoid overhead (it's instant)

		val result = Shuttle(context, to = target).invokeNoThrows(with = source as ApplicationInfo) {
			performAppCloningInProfile(this, it, mode == MODE_PLAY_STORE) }
		Log.i(TAG, "Result of cloning $pkg to $target: $result")

		when (result) {
			CLONE_RESULT_OK_INSTALL -> {
				// As visual feedback, since installation may take some time. TODO: Track installing apps with PackageInstaller
				IslandAppListProvider.getInstance(context).addPlaceholder(pkg, target)
				analytics().event("clone_install").with(Analytics.Param.ITEM_ID, pkg).send() }

			CLONE_RESULT_OK_INSTALL_EXISTING -> analytics().event("clone_install_existing").with(Analytics.Param.ITEM_ID, pkg).send()
			CLONE_RESULT_OK_GOOGLE_PLAY ->      analytics().event("clone_via_play").with(Analytics.Param.ITEM_ID, pkg).send()
			CLONE_RESULT_ALREADY_CLONED ->      Toast.makeText(context, R.string.toast_already_cloned, Toast.LENGTH_SHORT).show()
			CLONE_RESULT_NO_SYS_MARKET ->       showExplanation(activity, R.string.dialog_clone_incapable_explanation)

			CLONE_RESULT_UNKNOWN_SYS_MARKET -> {
				val info = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$pkg")).resolveActivityInfo(context.packageManager, 0)
				if (info != null && info.applicationInfo.isSystem)
					analytics().event("clone_via_market").with(Analytics.Param.ITEM_ID, pkg).with(Analytics.Param.ITEM_CATEGORY, info.packageName).send() }
			null -> Toast.makeText(context, R.string.prompt_island_not_ready, Toast.LENGTH_LONG).show() }
	}

	private fun isInstallerUsable() = ModuleContext(context).forDeclaredPermission(REQUEST_INSTALL_PACKAGES) != null

	private fun showExplanation(context: Context, textResId: Int) {
		val activity = Activities.findActivityFrom(context)
		if (activity != null) Dialogs.buildAlert(activity, 0, textResId)
			.setNeutralButton(com.oasisfeng.island.shared.R.string.action_learn_more) { _, _ ->
				WebContent.view(context, Config.URL_FAQ.get()) }
			.setPositiveButton(android.R.string.cancel, null).show()
		else Toast.makeText(context, textResId, Toast.LENGTH_LONG).show()
	}

	@OwnerUser @RequiresApi(O) private suspend fun cloneAppViaRoot(context: Context, source: IslandAppInfo, profile: UserHandle): Boolean {
		val pkg = source.packageName
		val cmd = "cmd package install-existing --user ${profile.toId()} $pkg"  // Try root approach first
		val result = withContext(Dispatchers.Default) { Shell.SU.run(cmd) }
		try {
			val app = context.getSystemService<LauncherApps>()!!.getApplicationInfo(pkg, 0, profile)
			if (app != null && app.installed) {
				Toast.makeText(context, context.getString(R.string.toast_successfully_cloned, source.label), Toast.LENGTH_SHORT).show()
				return true }}
		catch (e: NameNotFoundException) {
			Log.i(TAG, "Cannot clone app via root: $pkg")
			if (result != null && result.isNotEmpty()) analytics().logAndReport(TAG, "Error executing: $cmd",
					ExecutionException("ROOT: " + cmd + ", result: " + java.lang.String.join(" \\n ", result), null)) }
		return false
	}

	private fun isPlayStoreAvailableInProfiles(targets: Collection<UserHandle>) =
		targets.all { LauncherAppsCompat(context).getApplicationInfoNoThrows(GooglePlayStore.PACKAGE_NAME, 0, it) != null }

	private fun isKnownInstalledByPlayStore(context: Context, pkg: String) =
		if (SDK_INT >= VERSION_CODES.R) try {
			context.packageManager.getInstallSourceInfo(pkg).run {
				GooglePlayStore.PACKAGE_NAME.let { it == initiatingPackageName && it == installingPackageName }}}
			catch (e: NameNotFoundException) { false }
		else @Suppress("DEPRECATION") try { context.packageManager.getInstallerPackageName(pkg) == GooglePlayStore.PACKAGE_NAME }
			catch (e: IllegalArgumentException) { false }

	companion object {
		private const val CLONE_RESULT_ALREADY_CLONED = 0
		private const val CLONE_RESULT_OK_INSTALL = 1
		private const val CLONE_RESULT_OK_INSTALL_EXISTING = 2
		private const val CLONE_RESULT_OK_GOOGLE_PLAY = 10
		private const val CLONE_RESULT_UNKNOWN_SYS_MARKET = 11
		private const val CLONE_RESULT_NO_SYS_MARKET = -1

		@IntDef(MODE_INSTALLER, MODE_PLAY_STORE, MODE_SHIZUKU) @Target(TYPE) @Retention(SOURCE)
		annotation class AppCloneMode
		const val MODE_INSTALLER = 0
		const val MODE_PLAY_STORE = 1
		const val MODE_SHIZUKU = 2

		@ProfileUser private fun performAppCloningInProfile(context: Context, app: ApplicationInfo, viaPlayStore: Boolean): Int {
			val policies = DevicePolicies(context)
			if (policies.isProfileOwner)
				policies.clearUserRestrictionsIfNeeded(UserManager.DISALLOW_INSTALL_APPS)

			val pkg = app.packageName
			if (SDK_INT >= P && policies.manager.isAffiliatedUser) try {
				if (policies.invoke(DevicePolicyManager::installExistingPackage, pkg))
					return CLONE_RESULT_OK_INSTALL_EXISTING
				Log.e(TAG, "Error cloning existent user app: $pkg")     // Fall-through
			} catch (e: RuntimeException) { analytics().logAndReport(TAG, "Error cloning existent user app: $pkg", e) } // Fall-through

			if (! IslandManager.ensureLegacyInstallNonMarketAppAllowed(context, policies)) {    // Fallback to install via app store
				val marketIntent = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$pkg")).addFlags(FLAG_ACTIVITY_NEW_TASK)
				policies.enableSystemAppByIntent(marketIntent)
				val marketApp = context.packageManager.resolveActivity(marketIntent, MATCH_SYSTEM_ONLY)?.activityInfo?.applicationInfo
				return when {
					marketApp == null || ! marketApp.isSystem -> CLONE_RESULT_NO_SYS_MARKET
					marketApp.packageName != WellKnownPackages.PACKAGE_GOOGLE_PLAY_STORE -> CLONE_RESULT_UNKNOWN_SYS_MARKET.also {
						context.startActivity(marketIntent) }
					else -> CLONE_RESULT_OK_GOOGLE_PLAY.also {
						policies.enableSystemApp(WellKnownPackages.PACKAGE_GOOGLE_PLAY_SERVICES)     // Special dependency
						context.startActivity(marketIntent) }}}

			if (viaPlayStore && ensurePlayStoreReady(context, policies)) try {
				context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$pkg"))
					.setPackage(GooglePlayStore.PACKAGE_NAME).addFlags(FLAG_ACTIVITY_NEW_TASK))
				return CLONE_RESULT_OK_GOOGLE_PLAY
			} catch (e: ActivityNotFoundException) { Log.e(TAG, "Error launching Google Play Store to clone $pkg", e) }

			@Suppress("DEPRECATION") val intent = Intent(Intent.ACTION_INSTALL_PACKAGE, Uri.fromParts("package", pkg, null))
					.putExtra(EXTRA_INSTALLER_PACKAGE_NAME, context.packageName).addFlags(FLAG_ACTIVITY_NEW_TASK)
					.putExtra(InstallerExtras.EXTRA_APP_INFO, app).addCategory(context.packageName) // Launch App Installer
			if (policies.isProfileOwner) policies.enableSystemAppByIntent(intent)
			context.startActivity(intent)
			return CLONE_RESULT_OK_INSTALL
		}

		private fun ensurePlayStoreReady(context: Context, policies: DevicePolicies): Boolean {
			val pkg = GooglePlayStore.PACKAGE_NAME
			val info = try { context.packageManager.getApplicationInfo(pkg, MATCH_UNINSTALLED_PACKAGES or MATCH_DISABLED_COMPONENTS) }
				catch (e: NameNotFoundException) { return false }
			if (! info.enabled) return false    // We cannot enable a disabled system app
			if (! info.installed) return policies.enableSystemApp(pkg)
			if (info.hidden) policies.setApplicationHidden(pkg, false)
			if (info.suspended) policies.invoke(DPM::setPackagesSuspended, arrayOf(pkg), false).isEmpty()
			return true
		}
	}

	private val pkg = app.packageName
	private val context = app.context()
}

private const val TAG = "Island.AC"
